# cljs-electron

A Leiningen template for skeleton App in Electron.

[![Clojars Project](https://img.shields.io/clojars/v/cljs-electron/lein-template.svg)](https://clojars.org/cljs-electron/lein-template)

Using:
* re-frame "1.0.0"
* cljsjs/material-ui "4.10.2-0"
* cljsjs/material-ui-icons "4.4.1-0"
* cljsjs/mousetrap-plugins "1.6.3-0"
* React "16.13.1"
* electron "9.2.0"
* markdown-to-hiccup "0.6.2"

Also has:
* Update mechanism (only gitlab)
* Shortcuts
* Worker for long-running task
* .gitlab-ci.yml for building and releasing of new version

## License

Copyright © 2020 MIT

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
