(ns leiningen.new.cljs-electron
  (:require [leiningen.new.templates :refer [renderer name-to-path ->files *force?*]]
            [leiningen.core.main :as main]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [clj-jgit.porcelain :as git]))

(def render (renderer "cljs-electron"))

(defn binary [file]
  (io/input-stream (io/resource (string/join "/" ["leiningen" "new" "cljs_electron" file]))))

(defn main-files [files-vec data]
  (let [base-path "src/main/{{sanitized}}/"]
    (conj files-vec
          [(str base-path "main_core.cljs") (render "main/core.cljs" data)]
          [(str base-path "auto_updater/api.cljs") (render "main/auto-updater/api.cljs" data)]
          [(str base-path "auto_updater/config.cljs") (render "main/auto-updater/config.cljs" data)]
          [(str base-path "auto_updater/core.cljs") (render "main/auto-updater/core.cljs" data)]
          [(str base-path "auto_updater/util.cljs") (render "main/auto-updater/util.cljs" data)])))

(defn ui-files [files-vec data]
  (let [base-path "src/ui/{{sanitized}}/"
        comp (str base-path "components/")
        paths (str base-path "paths/")
        utils (str base-path "utils/")
        views (str base-path "views/")]
    (conj files-vec
          ;Components
          [(str comp "confirm_dialog.cljs") (render "ui/components/confirm_dialog.cljs" data)]
          [(str comp "core.cljs") (render "ui/components/core.cljs" data)]
          [(str comp "loading.cljs") (render "ui/components/loading.cljs" data)]
          [(str comp "status.cljs") (render "ui/components/status.cljs" data)]
          [(str comp "tabs.cljs") (render "ui/components/tabs.cljs" data)]
          ;Paths
          [(str paths "components.cljs") (render "ui/paths/components.cljs" data)]
          [(str paths "shortcuts.cljs") (render "ui/paths/shortcuts.cljs" data)]
          [(str paths "tabs.cljs") (render "ui/paths/tabs.cljs" data)]
          [(str paths "utils.cljs") (render "ui/paths/utils.cljs" data)]
          [(str paths "views.cljs") (render "ui/paths/views.cljs" data)]
          ;utils
          [(str utils "shortcuts.cljs") (render "ui/utils/shortcuts.cljs" data)]
          [(str utils "system_conditions.cljs") (render "ui/utils/system_conditions.cljs" data)]
          [(str utils "things.cljs") (render "ui/utils/things.cljs" data)]
          ;views
          [(str views "settings/core.cljs") (render "ui/views/settings/core.cljs" data)]
          [(str views "settings/general.cljs") (render "ui/views/settings/general.cljs" data)]
          [(str views "settings/util.cljs") (render "ui/views/settings/util.cljs" data)]
          [(str views "main_view.cljs") (render "ui/views/main_view.cljs" data)]
          [(str views "shortcut_view.cljs") (render "ui/views/shortcut_view.cljs" data)]
          [(str views "starting.cljs") (render "ui/views/starting.cljs" data)]
          [(str views "welcome.cljs") (render "ui/views/welcome.cljs" data)]
          ;root
          [(str base-path "config.cljs") (render "ui/config.cljs" data)]
          [(str base-path "material_ui.cljs") (render "ui/material_ui.cljs" data)]
          [(str base-path "settings.cljs") (render "ui/settings.cljs" data)]
          [(str base-path "ui_core.cljs") (render "ui/ui_core.cljs" data)]
          [(str base-path "update_ui.cljs") (render "ui/update_ui.cljs" data)]
          [(str base-path "worker.cljs") (render "ui/worker.cljs" data)])))

(defn shared-files [files-vec data]
  (let [base-path "src/shared/{{sanitized}}/"]
    (conj files-vec
          [(str base-path "data.cljs") (render "shared/data.cljs" data)]
          [(str base-path "util.cljs") (render "shared/util.cljs" data)])))

(defn worker-files [files-vec data]
  (let [base-path "src/worker/{{sanitized}}/"]
    (conj files-vec
          [(str base-path "utils/queue.cljs") (render "worker/utils/queue.cljs" data)]
          [(str base-path "example.cljs") (render "worker/example.cljs" data)]
          [(str base-path "worker_core.cljs") (render "worker/worker_core.cljs" data)]
          [(str base-path "worker_settings.cljs") (render "worker/worker_settings.cljs" data)])))

(defn update-helper-files [files-vec data]
  (let [base-path "update-helper/"]
    (conj files-vec
          [(str base-path "src/update_helper/core.clj") (render "update-helper/src/update_helper/core.clj" data)]
          [(str base-path "src/update_helper/downloader.clj") (render "update-helper/src/update_helper/downloader.clj" data)]
          [(str base-path "src/update_helper/swing_ui.clj") (render "update-helper/src/update_helper/swing_ui.clj" data)]
          [(str base-path "src/update_helper/ui_config.clj") (render "update-helper/src/update_helper/ui_config.clj" data)]
          [(str base-path "src/update_helper/unzipper.clj") (render "update-helper/src/update_helper/unzipper.clj" data)]
          [(str base-path "project.clj") (render "update-helper/project.clj" data)])))

(defn ressource-files [files-vec data]
  (let [base-path "resources/public/"]
    (conj files-vec
          ;CSS
          [(str base-path "css/material_font.css") (render "public/css/material_font.css" data)]
          [(str base-path "css/material_icon.css") (render "public/css/material_icon.css" data)]
          [(str base-path "css/ownstyles.css") (render "public/css/ownstyles.css" data)]
          ;fonts
          [(str base-path "fonts/glyphicons-halflings-regular.ttf") (binary "public/fonts/glyphicons-halflings-regular.ttf")]
          [(str base-path "fonts/glyphicons-halflings-regular.woff") (binary "public/fonts/glyphicons-halflings-regular.woff")]
          [(str base-path "fonts/glyphicons-halflings-regular.woff2") (binary "public/fonts/glyphicons-halflings-regular.woff2")]
          ;img
          [(str base-path "img/icon-html.png") (binary "public/img/icon-html.png")]
          [(str base-path "img/logo.png") (binary "public/img/logo.png")]
          [(str base-path "img/splashscreen_loading.gif") (binary "public/img/splashscreen_loading.gif")]
          ;root
          [(str base-path "index.html") (render "public/index.html" data)]
          [(str base-path "loading.html") (render "public/loading.html" data)]
          [(str base-path "update-config.json") (render "public/update-config.json" data)]
          [(str base-path "worker.html") (render "public/worker.html" data)])))

(defn root-files [files-vec data]
  (conj files-vec
        [".gitignore" (render ".gitignore" data)]
        [".gitlab-ci.yml" (render ".gitlab-ci.yml" data)]
        ["Makefile" (render "Makefile" data)]
        ["package.json" (render "package.json" data)]
        ["project.clj" (render "project.clj" data)]
        ["README.md" (render "README.md" data)]))

(defn cljs-electron
  "Create a new cljs-electron project"
  [name]
  (let [data {:name name
              :sanitized (name-to-path name)}
        files-args (-> [data]
                       (root-files data)
                       (ressource-files data)
                       (update-helper-files data)
                       (main-files data)
                       (ui-files data)
                       (worker-files data)
                       (shared-files data))]
    (main/info "Generating fresh 'lein new' cljs-electron project.")
    (main/info (str "... Setting up git repository."))
    (doto (git/git-init :dir name)
      (git/git-commit (str "Initializing new electron-app '" name "'\n\nCreated using cljs-electron template.") :allow-empty? true)
      (git/git-tag-create "v0.0.0"
                          :message "Initial version number"))
    (main/info (str "... Populating files."))
    (binding [*force?* true]
      (apply ->files files-args)))) 
