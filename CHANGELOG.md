# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## [Unreleased]
### Changed


## [0.1.1] - 2018-10-19
### Changed
- Fixed to quit worker when reloading/closing main-app

## 0.1.0 - 2018-10-19
### Added
- Init Release
  
[Unreleased]: https://github.com/your-name/cljs-electron/compare/0.1.1...HEAD
[0.1.1]: https://github.com/your-name/cljs-electron/compare/0.1.0...0.1.1
