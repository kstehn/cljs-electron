(ns {{name}}.config
  (:require [electron :refer [remote]]
            [clojure.string :as string]))

(def app-name "{{name}}")
(def full-version (.getVersion (aget remote "app")))
(def version (string/split full-version #"\.")) ;Automatically gets App version but only working in production mode
(def main-version (string/join "." (take 2 version)))
(def minor-version (string/join "." (drop 2 version)))
