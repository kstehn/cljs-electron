(ns {{name}}.update-ui
 (:require [re-frame.core :as re-frame]
           [{{name}}.components.core :as components]
           [{{name}}.material-ui :as mui]
           [markdown-to-hiccup.core :as m]
           [{{name}}.util :refer [send-ipc]]))

(re-frame/reg-event-fx
 ::start-update
 (fn [_ _]
   (js/console.log "STARTING UPDATE")
   (send-ipc "install-update")
   {}))

(re-frame/reg-sub
 ::update-available?
 (fn [db]
   (get db ::update-available)))

(re-frame/reg-sub
 ::update-checking-running?
 (fn [db]
   (get db ::update-checkin-running false)))

(re-frame/reg-event-db
 ::run-check
 (fn [db [_ update-func callbackvec]]
   (update-func)
   (assoc db ::update-checkin-running true
             ::callback callbackvec)))

(re-frame/reg-event-db
 ::reset-update-params
 (fn [db]
  (dissoc db ::update-checkin-running
             ::update-available
             ::callback)))

(re-frame/reg-event-fx
 ::checking-results
 (fn [{db :db} [_ update-infos]]
    (if update-infos
     (do
      {:db (assoc db ::update-available (assoc update-infos
                                               :callback (get db ::callback)))}) ;Um ein Sub zu sparen

     (let [callback (get db ::callback)]
      {:db (-> (assoc db ::update-available false)
               (dissoc ::update-checkin-running
                       ::callback))
       :dispatch-n [(when callback
                     callback)
                    (when callback
                     [::reset-update-params])]}))))

(defn update-available
  "Gets executed when a new update is available"
  [event update-infos]
  (let [update-infos (js->clj update-infos :keywordize-keys true)]
   (re-frame/dispatch [::checking-results 
                       (when update-infos (not-empty update-infos)
                             update-infos)]))) 

(re-frame/reg-event-fx
 ::update-failed
 (fn [{db :db}]
  (let [callback (or (get-in db [::update-available :callback])
                     (get db ::callback))]
   {:db (dissoc db ::update-available ::callback)
    :dispatch [:{{name}}.components.confirm-dialog/show-dialog
               true
               (or callback [::reset-update-params])
               "Update failed"
               "Update failed. Reasons can be: your internet connection or broken release."
               [::reset-update-params]
               {:confirm-label "Ok"
                :abort-button false}]})))

(defn update-failed
  "Gets executed when installation of Update failed"
  [event]
  (re-frame/dispatch [::update-failed]))

(defn currversion-is-newest
  "Gets executed when no newer version is available"
  [event]
  (re-frame/dispatch [::checking-results false #_{:version "alpha-2423-1212" ;Test
                                                  :release-notes "  * Test"}]))

(defn release-view [update-infos show-buttons?]
 (let [{:keys [release-notes version callback]} update-infos
       release-notes (->> release-notes
                          (m/md->hiccup)
                          (m/component))]
  [:div {:style {:margin-top "20px"
                 :margin-bottom "20px"}}
   [mui/typography {:style {:color "rgb(51,153,153)"}}
    "New in this version " version ":"]
   [:div {:style {:width "450px"
                  :margin-top "10px"
                  :margin-bottom "10px"
                  :border "1px solid rgba(51,153,153,0.7)"
                  :text-align :left}}
    release-notes]
   [mui/typography {:style {:color "rgb(51,153,153)"}}
    "Do you want to update?"]
   (when show-buttons?
     [mui/button {:color "secondary"
                  :variant "outlined"
                  :size :small
                  :style {:width "100px"
                          :margin-top "20px"
                          :margin-right "30px"}
                  :on-click #(do
                               (when callback
                                 (re-frame/dispatch callback)))}
      "No"])
   (when show-buttons?
     [mui/button {:color "primary"
                  :variant "outlined"
                  :size :small
                  :style {:width "100px"
                          :margin-top "20px"}
                  :on-click #(re-frame/dispatch [::start-update])}
      "Yes"])]))

(defn update-view
 ([]
  [update-view true])
 ([show-buttons?]
  (let [update-infos @(re-frame/subscribe [::update-available?])]
    (cond
     update-infos [release-view update-infos show-buttons?]
     (= update-infos false) [mui/typography {:style {:color "rgb(51,153,153)"}}
                             "Newest version already installed :)"]
     :else [components/loading "Check for newer version..."]))))

(re-frame/reg-event-fx
 ::run-check-dialog
 (fn [{db :db} [_ update-func callbackvec]]
   (update-func)
   {:db (assoc db ::update-checkin-running true
                  ::callback callbackvec)
    :dispatch [:{{name}}.components.confirm-dialog/show-dialog
                true
               [::start-update]
               "Update check"
               [:center [update-view false]]
               [::reset-update-params]
               {:confirm-label "Update"
                :abortlabel "Cancel"
                :confirm-deactivated (fn [] (if @(re-frame/subscribe [::update-available?])
                                              false
                                              true))}]}))
