(ns {{name}}.components.tabs
  (:require [{{name}}.material-ui :as mui]
            [re-frame.core :as re-frame]))

(defn component [{:keys [selected-sub possible-tabs-sub disabled-sub select-event min-width] :as props}]
  (if-not (or selected-sub possible-tabs-sub select-event)
    (js/console.error "Invalid props of Tabs component" props)
    (let [selected @(re-frame/subscribe selected-sub)
          possible-tabs @(re-frame/subscribe possible-tabs-sub)]
      [mui/paper {:square false}
       [:center
        (apply conj
               [mui/tabs {:textColor "secondary"
                          :indicatorColor "secondary"
                          :style {:min-height "0px"}
                          :value selected
                          :variant :scrollable
                          :on-change (fn [event value]
                                       (re-frame/dispatch (conj select-event value)))}]
               (mapv (fn [label]
                       (let [disabled (when disabled-sub @(re-frame/subscribe (conj disabled-sub label)))]
                         [mui/tab (cond-> {:label label
                                           :value label
                                           :style {:min-width (or min-width "100px")
                                                   :font-size "12px"
                                                   :min-height "0px"}}
                                    disabled-sub (assoc :disabled disabled))]))
                     possible-tabs))]])))