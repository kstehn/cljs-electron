(ns {{name}}.components.core
  (:require [{{name}}.components.loading :as loading]
            [{{name}}.components.status :as status]
            [{{name}}.components.tabs :as tabs]))

(def loading loading/component)

(def status status/component)

(def tabs tabs/component)
