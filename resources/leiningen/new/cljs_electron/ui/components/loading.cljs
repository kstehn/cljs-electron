(ns {{name}}.components.loading
  (:require [{{name}}.material-ui :as mui]))

(defn component [message]
 [:div
  [:div.loader
   [:div.loader__bar]
   [:div.loader__bar]
   [:div.loader__bar]
   [:div.loader__bar]
   [:div.loader__bar]
   [:div.loader__ball]]
  [mui/typography {:variant "subtitle2"}
   message]])
