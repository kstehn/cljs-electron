(ns {{name}}.components.status
  (:require [{{name}}.material-ui :as mui]
            [re-frame.core :as re-frame]
            [reagent.core :as reagent]
            [{{name}}.paths.components :as paths]
            [{{name}}.utils.things :refer [get-unique-dom-key]]))

(re-frame/reg-event-db
 ::show-status-message
 (fn [db [_ message type duration]]
   (assoc-in db paths/status-message {:show? true
                                      :id (str (random-uuid) message)
                                      :message message
                                      :duration duration
                                      :type type})))

(re-frame/reg-event-db
 ::close-status
 (fn [db [_ id]]
   (if (= id (get-in db (conj paths/status-message :id))) ;Check if new message arrived since waiting duration
     (-> db
         (update-in paths/status-message dissoc :show?)
         (assoc-in paths/status-exiting true))
     db)))

(re-frame/reg-event-db
 ::status-finished
 (fn [db]
   (update-in db paths/status dissoc paths/status-message-key)))

(re-frame/reg-sub
 ::show-status-message?
 (fn [db]
   (get-in db paths/status-message)))

(defn choose-type-icon [type]
  (case type
    :success [mui/icon "CheckCircle" {:fontSize :small
                                      :style {:margin-right "10px"}}]
    :error [mui/icon "Error" {:fontSize :small
                              :style {:margin-right "10px"}}]
    :warn [mui/icon "Warning" {:fontSize :small
                               :style {:margin-right "10px"}}]
    [mui/icon "Info" {:fontSize :small
                      :style {:margin-right "10px"}}]))

(defn component []
  (let [{:keys [show? exiting message type duration id]
         :or {show? false
              exiting false
              message ""
              type ""}} @(re-frame/subscribe [::show-status-message?])
        duration (or duration 2000)
        color (case type
                :success "#43a047"
                :error "#d32f2f"
                :warn "#ffa000"
                "#1976d2")
        dom-key (get-unique-dom-key)]
    [mui/snackbar {:open show?
                   :key (str dom-key "status")
                     ;:style {:box-shadow (when show? "0px 2px 4px -1px rgba(0, 0, 0, 0.2),0px 4px 5px 0px rgba(0, 0, 0, 0.14),0px 1px 10px 0px rgba(0, 0, 0, 0.12)")}
                   :autoHideDuration 0
                   :disableWindowBlurListener true
                   :anchorOrigin {:vertical "bottom"
                                  :horizontal "right"}
                   :on-close (fn [] (js/setTimeout #(re-frame/dispatch [::close-status id])
                                                   duration)) ;Fix for flickering (caused by react rerender in combination with subs)                     
                   :on-exited #(re-frame/dispatch [::status-finished])
                   :transitionDuration {:enter 250
                                        :exit 250}
                   :TransitionProps {:direction (if (not exiting)
                                                  "left"
                                                  "down")
                                     :in (not exiting)
                                     :exit true}}

     [mui/snackbar-content   {:key (str dom-key "status-content")
                              :style {:backgroundColor color}
                              :aria-describedby "status-snackbar-content"
                              :action [(reagent/as-element [mui/icon-button {:key (str dom-key "close-button")
                                                                             :aria-label "close"
                                                                             :color "inherit"
                                                                             :on-click #(re-frame/dispatch [::close-status])}
                                                            [mui/icon "Close" {:fontSize :small}]])]

                              :message (reagent/as-element [:span {:key (str dom-key "type-icon")
                                                                   :style {:display :flex
                                                                           :alignItems "center"}

                                                                   :id "status-snackbar-content"}
                                                            [choose-type-icon type]
                                                            message])}]]))

