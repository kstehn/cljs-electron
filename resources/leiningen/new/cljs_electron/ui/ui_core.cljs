(ns {{name}}.ui-core
  "Houses application state and defines actions and the main application component"
  (:require [reagent.dom :as rdom]
            [{{name}}.utils.shortcuts :as shortcuts]
            [re-frame.core :as re-frame]
            [{{name}}.util :refer [on-ipc get-url send-ipc destroy-current]]
            [{{name}}.update-ui :as upd-ui]
            [{{name}}.worker :as worker]
            [{{name}}.config :as config]
            [{{name}}.views.main-view :as main-view]
            [{{name}}.views.welcome]
            [{{name}}.views.settings.core]
            [{{name}}.views.starting :as starting]
            [{{name}}.utils.system-conditions]))

(on-ipc "system-conditions" (fn [_ res] (re-frame/dispatch [::starting/system-conditions res])))
(on-ipc "settings-loaded" starting/settings-loaded)
(on-ipc "update-available" upd-ui/update-available)
(on-ipc "update-install-error" upd-ui/update-failed)
(on-ipc "currversion-is-newest" upd-ui/currversion-is-newest)
(on-ipc "example-done" (fn [_ res]
                         (js/console.log "Example done" 
                                         (js->clj res :keywordize-keys true))))

(re-frame/reg-event-db
 ::initialize-db
 (fn  [_ _]
   {}))

(re-frame/reg-event-db
 ::final-close
 (fn [_]
   (worker/close-worker!)
   (destroy-current)))

(defn ^:export init []
  (re-frame/dispatch-sync [::initialize-db])
  (re-frame/clear-subscription-cache!)
  (worker/message! "example")
  (.addEventListener js/window "beforeunload"
                     (fn [event]
                       #_(do (re-frame/dispatch [:{{name}}.components.confirm-dialog/show-dialog
                                                 true
                                                 {:fire-event [::final-close]
                                                  :dialog-title "Closing?"
                                                  :dialog-content-text "You still have unsaved changes. Do you really want to close {{name}}?"}])
                             (aset event "returnValue" false)) ;Do this when you need to do something before closing: example open dialog
                       (worker/close-worker!)))
  (rdom/render
   [main-view/{{name}}]
   (.getElementById js/document "{{name}}"))
  (shortcuts/register-all)
  (send-ipc "get-system-conditions"))

;;; Render {{name}} as node application
(defn -main []
  (init))

(set! *main-cli-fn* -main)
