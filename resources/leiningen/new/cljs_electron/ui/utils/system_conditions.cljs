(ns {{name}}.utils.system-conditions
  (:require [re-frame.core :as re-frame]
            [{{name}}.paths.utils :as paths]))

(def mac-identifier "darwin")

(re-frame/reg-event-db
 ::system-conditions
 (fn [db [_ system-conditions]]
  (assoc-in db paths/system-conditions system-conditions)))

(defn is-mac? [os]
  (= os mac-identifier))

(re-frame/reg-sub
 ::updatefeature-available?
 (fn [db]
  (and (get-in db paths/java false)
       (not (is-mac? (get-in db paths/os mac-identifier))))))

(re-frame/reg-sub
 ::is-mac?
 (fn [db]
   (is-mac? (get-in db paths/os mac-identifier))))

(re-frame/reg-sub
 ::java-available?
 (fn [db]
  (get-in db paths/java false)))
