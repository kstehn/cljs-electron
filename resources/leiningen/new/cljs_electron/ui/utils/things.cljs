(ns {{name}}.utils.things)

(def key-counter (atom 0))

(defn get-unique-dom-key []
  (reset! key-counter (inc @key-counter)))

(defn sum-by-key [collec value-path]
 (reduce (fn [msum entry]
           (+ msum (get-in entry value-path 0)))
        0
        collec))
