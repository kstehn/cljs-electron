(ns {{name}}.utils.shortcuts
  (:require cljsjs.mousetrap-plugins
            [reagent.core :as r]
            [clojure.string :as clj-str]
            [re-frame.core :as re-frame]))

(defn switch-view-trigger-fn [label key]
  (fn []
    (let [view @(re-frame/subscribe [:{{name}}.views.main-view/current-view])]
      (when-not (or (= key view)
                    (= label view))
        (re-frame/dispatch [:{{name}}.views.main-view/switch-view :settings]))
      false)))

(defn open-keybinds-trigger-fn []
  (when-not  @(re-frame/subscribe [:{{name}}.views.shortcut-view/show-dialog?])
    (re-frame/dispatch [:{{name}}.views.shortcut-view/show-dialog true]))
  false)

(def default-shortcuts
  {:switch-to-settings {:bind "ctrl+2"
                        :label "Switch to settings"
                        :section :general
                        :order 80
                        :trigger-fn (switch-view-trigger-fn "Settings" :settings)}
   :switch-to-welcome {:bind ""
                       :label "Switch to welcome"
                       :section :general
                       :order 70
                       :trigger-fn (switch-view-trigger-fn "Welcome" :welcome)}
   :open-keybindings {:bind "ctrl+3"
                      :label "Open shortcuts"
                      :section :general
                      :order 90
                      :trigger-fn open-keybinds-trigger-fn}})

(def shortcuts (r/atom default-shortcuts))

(defn get-shortcuts []
  @shortcuts)

(defn extract-bindings [shortcut-map]
  (reduce (fn [res [k {:keys [bind]}]]
            (assoc res k bind))
          {}
          shortcut-map))

(defn get-default-bindings []
  (extract-bindings default-shortcuts))

(defn register-all []
  (.reset js/Mousetrap)
  (aset js/Mousetrap.prototype "stopCallback" (fn [e element combo]
                                                false))
  (doseq [{:keys [bind trigger-fn]} (vals @shortcuts)]
    (js/Mousetrap.bind bind trigger-fn)))

(defn stop []
  (.reset js/Mousetrap))

(defn resume []
  (register-all))

(defn record-shortcut [callback]
  (js/Mousetrap.record
   (fn [sequence]
     (callback (clj-str/join " " sequence)))))

(defn is-same-binding? [shortcut-id new-bind]
  (= new-bind (get-in @shortcuts [shortcut-id :bind])))

(defn binding-exist?
  ([check-bind]
   (binding-exist? check-bind nil))
  ([check-bind exclude-ids]
   (some #(and
           (or (nil? exclude-ids)
               (not (some (fn [id]
                            (= (get % 0) id))
                          exclude-ids)))
           (= check-bind (get-in % [1 :bind])))
         @shortcuts)))

(defn set-binding [shortcut-id new-binding]
  (let [already-exist? (binding-exist? new-binding)]
    (when-not already-exist?
      (swap! shortcuts assoc-in [shortcut-id :bind] new-binding))))

(defn set-bindings [bindings]
  (doseq [[id bind] bindings]
    (set-binding id bind)))

(re-frame/reg-event-db
 ::restore-keybindings
 (fn [db [_ bindings]]
   (set-bindings bindings)
   (register-all)
   db))

(defn save-bindings [bindings]
  (set-bindings bindings)
  (re-frame/dispatch [:{{name}}.views.settings.core/save-settings-node
                      :keybindings
                      bindings]))
