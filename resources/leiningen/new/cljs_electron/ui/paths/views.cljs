(ns {{name}}.paths.views)

(def root-key :views)

(def all-views [root-key :all])

;==== App Navigation ====
(def navigation-root :navigation)
(def show-navigation-key :show?)
(def current-view-key :current-view)
(def navigation [navigation-root show-navigation-key])
(def current-view [navigation-root current-view-key])

;==== View Description ====
(def view-id-key :id)
(def view-label-key :label)
(def view-icon-key :icon)
(def view-content-key :content)
(def display-fn-key :display-fn)
(def view-order-key :order)