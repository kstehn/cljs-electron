(ns {{name}}.paths.shortcuts)

;====== Paths =======
(def root :shortcuts)
(def show-key :show)
(def edit-key :edit)

(def shortcuts-root [root])
(def show [root show-key])
(def edit-root [root edit-key])
(defn edit [shortcut-id]
  (conj edit-root shortcut-id))