(ns {{name}}.paths.components
    (:require [{{name}}.paths.views :as views-paths]))

;==== Status ====
(def status-root :status)
(def status [views-paths/root-key status-root])
(def status-message-key :status-message)
(def status-message (conj status status-message-key))
(def status-exiting (conj status-message :exiting))

;==== Confirm dialog ====
(def dialog-key :dialog)
(def confirm [:main :confirm])
(def dialog (conj confirm dialog-key))
(def dialog-show? (conj dialog :show?))