(ns {{name}}.paths.tabs)

;==== Tab Description ====
(def tab-id-key :id)
(def tab-content-key :content)
(def tab-request-key :request)
(def selection-subscription :sel-sub)
(def tab-label-key :label)

(defn tab-id [index]
  [index tab-id-key])
(defn tab-content [index]
  [index tab-content-key])
(defn tab-request [index]
  [index tab-request-key])
(defn tab-label [index]
  [index tab-label-key])
(defn tab-selection-subscription [index]
  [index selection-subscription])

;===== Request-Params =====
(def request-key :tabs-request)
(def year-key :year)
(def month-key :month)
(def week-key :week)
(def day-key :day)

(def request [request-key])
(def request-year (conj request year-key))
(def request-month (conj request month-key))
(def request-week (conj request week-key))
(def request-day (conj request day-key))
