(ns {{name}}.paths.utils)

(def system-conditions-key :sys-conds)
(def system-conditions [system-conditions-key])

;====  Java ====
(def java-key :java-available?)
(def java (conj system-conditions java-key))

;====  OS ====
(def os-key :os)
(def os (conj system-conditions os-key))
