(ns {{name}}.material-ui
  (:require cljsjs.material-ui
            material-ui-icons
            [reagent.core :as r]
            [re-frame.core :as re-frame]))

(def google-ui js/MaterialUI)
(def google-ui-icons js/MaterialUIIcons)

(def paper (r/adapt-react-class (aget google-ui "Paper")))
(def dialog (r/adapt-react-class (aget google-ui "Dialog")))
(def dialog-title (r/adapt-react-class (aget google-ui "DialogTitle")))
(def dialog-content (r/adapt-react-class (aget google-ui "DialogContent")))
(def dialog-content-text (r/adapt-react-class (aget google-ui "DialogContentText")))
(def dialog-actions (r/adapt-react-class (aget google-ui "DialogActions")))

(def stepper (r/adapt-react-class (aget google-ui "Stepper")))
(def step (r/adapt-react-class (aget google-ui "Step")))
(def step-label (r/adapt-react-class (aget google-ui "StepLabel")))
(def step-content (r/adapt-react-class (aget google-ui "StepContent")))
(def avatar (r/adapt-react-class (aget google-ui "Avatar")))

(def collapse (r/adapt-react-class (aget google-ui "Collapse")))
(def linear-progress (r/adapt-react-class (aget google-ui "LinearProgress")))

(def drawer (r/adapt-react-class (aget google-ui "Drawer")))

(def divider (r/adapt-react-class (aget google-ui "Divider")))
(def mui-list (r/adapt-react-class (aget google-ui "List")))
(def listitem (r/adapt-react-class (aget google-ui "ListItem")))
(def listitem-icon (r/adapt-react-class (aget google-ui "ListItemIcon")))
(def listitem-text (r/adapt-react-class (aget google-ui "ListItemText")))

(def checkbox (r/adapt-react-class (aget google-ui "Checkbox")))

(def appbar (r/adapt-react-class (aget google-ui "AppBar")))
(def tabs (r/adapt-react-class (aget google-ui "Tabs")))
(def tab (r/adapt-react-class (aget google-ui "Tab")))
(def toolbar (r/adapt-react-class (aget google-ui "Toolbar")))

(def typography (r/adapt-react-class (aget google-ui "Typography")))
(def swipeable-drawer (r/adapt-react-class (aget google-ui "SwipeableDrawer")))

(def form-control (r/adapt-react-class (aget google-ui "FormControl")))
(def form-group (r/adapt-react-class (aget google-ui "FormGroup")))
(def form-control-label (r/adapt-react-class (aget google-ui "FormControlLabel")))
(def form-helper-text (r/adapt-react-class (aget google-ui "FormHelperText")))
(def input-label (r/adapt-react-class (aget google-ui "InputLabel")))
(def switch (r/adapt-react-class (aget google-ui "Switch")))
(def select-mui (r/adapt-react-class (aget google-ui "Select")))
(def menu (r/adapt-react-class (aget google-ui "Menu")))
(def menu-item (r/adapt-react-class (aget google-ui "MenuItem")))
(def outlined-input (r/adapt-react-class (aget google-ui "OutlinedInput")))
(def input (r/adapt-react-class (aget google-ui "Input")))

(defn disabled-menu-item [[label value w]]
  (let [menu-map {:key value
                  :value value
                  :disabled true}]
    [menu-item (if w
                 (assoc menu-map :style {:width w
                                         :min-height "25px"})
                 menu-item)
     label]))

(defn option->menu-item [[label value w]]
  (let [menu-map {:key value
                  :value value}]
    [menu-item (if w
                 (assoc menu-map :style {:width w
                                         :min-height "30px"})
                 menu-map)
     label]))

(defn vec->options 
  ([options label-key id-key]
   (if (or (nil? options)
           (empty? options))
     [(disabled-menu-item ["No options available" "no-options" 250])]
     (mapv (fn [opt]
             (let [label (if (string? opt)
                           opt
                           (get opt label-key))
                   key (if (string? opt)
                         opt
                         (get opt id-key))]
               (option->menu-item [label key 250])))
           options)))
  ([options]
   (vec->options options :label :key)))

(defn select-component [props]
  (fn [props]
    (let [{:keys [id defaultname name value is-valid-input? on-change-func options helper-text disabled multi render-value]} props
          select-map {:native false
                      :on-change on-change-func
                      :autoWidth true
                      :multiple (if (nil? multi)
                                  false
                                  multi)
                      :disabled (if (nil? disabled)
                                  false
                                  disabled)
                      :value value
                      :input (r/as-element [input {:name (if defaultname
                                                           defaultname
                                                           name)
                                                   :id id}])}]
                                                            ; :defaultValue value}])}]
      [form-control {:fullWidth true
                     :error (and (not (nil? is-valid-input?))
                                 (not is-valid-input?))}
       [input-label {:htmlFor id
                     :for id}
        name]
       (apply conj
              [select-mui (if render-value
                            (assoc select-map :render-value render-value)
                            select-map)]
              options)
       (when (and helper-text (not= helper-text ""))
         [form-helper-text
          helper-text])])))

(re-frame/reg-sub
 ::label-width
 (fn [db [_ id]]
   (get-in db [::label-width id] 0)))

(re-frame/reg-event-db
 ::label-width
 (fn [db [_ id width]]
   (assoc-in db [::label-width id] width)))

(defn with-label [label component]
  (fn [label component]
    [:div {:style {:display "flex"}}
     [typography {:variant "body1"
                  :style {:padding-top "20px"
                          :padding-right "15px"}}
      label]
     component]))

(def svg-icon (r/adapt-react-class (aget google-ui "SvgIcon")))
(def card (r/adapt-react-class (aget google-ui "Card")))
(def card-content (r/adapt-react-class (aget google-ui "CardContent")))
(def card-media (r/adapt-react-class (aget google-ui "CardMedia")))
(def card-actions (r/adapt-react-class (aget google-ui "CardActions")))

(def textfield (r/adapt-react-class (aget google-ui "TextField")))
(def input-adornment (r/adapt-react-class (aget google-ui "InputAdornment")))
(def button (r/adapt-react-class (aget google-ui "Button")))
(def icon-button (r/adapt-react-class (aget google-ui "IconButton")))

(def expansion-panel (r/adapt-react-class (aget google-ui "ExpansionPanel")))
(def expansion-panel-summary (r/adapt-react-class (aget google-ui "ExpansionPanelSummary")))
(def expansion-panel-details (r/adapt-react-class (aget google-ui "ExpansionPanelDetails")))
(def expansion-panel-actions (r/adapt-react-class (aget google-ui "ExpansionPanelActions")))

;(def toggle-button-group (aget google-ui "ToggleButtonGroup"))
;(def toggle-button (aget google-ui "ToggleButton"))
(def grid (r/adapt-react-class (aget google-ui "Grid")))

(def table (r/adapt-react-class (aget google-ui "Table")))
(def table-head  (r/adapt-react-class (aget google-ui "TableHead")))
(def table-body (r/adapt-react-class (aget google-ui "TableBody")))
(def table-row (r/adapt-react-class (aget google-ui "TableRow")))
(def table-cell (r/adapt-react-class (aget google-ui "TableCell")))
(def table-footer (r/adapt-react-class (aget google-ui "TableFooter")))
(def table-sort-label (r/adapt-react-class (aget google-ui "TableSortLabel")))
(def table-pagination (r/adapt-react-class (aget google-ui "TablePagination")))

(def tooltip  (r/adapt-react-class (aget google-ui "Tooltip")))
(def snackbar (r/adapt-react-class (aget google-ui "Snackbar")))
(def snackbar-content (r/adapt-react-class (aget google-ui "SnackbarContent")))
(def slide (r/adapt-react-class (aget google-ui "Slide")))
(def createMuiTheme (aget google-ui "createMuiTheme"))
(def mui-theme-provider (r/adapt-react-class (aget google-ui "MuiThemeProvider")))

(def badge (r/adapt-react-class (aget google-ui "Badge")))
(def chip (r/adapt-react-class (aget google-ui "Chip")))
(defn with-styles [styles classname]
  (.withStyles google-ui styles classname))

(def circular-progress (r/adapt-react-class (aget google-ui "CircularProgress")))

(def icon-cache (atom {}))

(defn icon [icon-name props]
  (if-let [i (get @icon-cache icon-name)]
    [i props]
    (let [i (r/adapt-react-class (aget google-ui-icons icon-name))]
      (swap! icon-cache assoc icon-name i)
      [i props])))