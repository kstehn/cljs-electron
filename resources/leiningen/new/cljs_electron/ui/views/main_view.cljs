(ns {{name}}.views.main-view
  (:require [re-frame.core :as re-frame]
            [{{name}}.views.shortcut-view :as shortcut-view]
            [{{name}}.paths.views :as paths]
            [{{name}}.components.core :as components]
            [{{name}}.material-ui :as mui]
            [{{name}}.config :as config]
            [{{name}}.update-ui :as upd-ui]
            [clojure.string :as clj-str]
            [{{name}}.util :refer [open-link]]))

(re-frame/reg-event-db
 ::start-ui
 (fn [db]
   (assoc-in db [:start-ui] true)))

(re-frame/reg-sub
 ::start-ui?
 (fn [db]
   (get-in db [:start-ui] false)))

(re-frame/reg-event-fx
 ::switch-view
 (fn [{db :db} [_ view]]
   {:db (assoc-in db paths/current-view view)}))

(re-frame/reg-sub
 ::current-view
 (fn [db _]
   (keyword (get-in db paths/current-view :settings))))

(re-frame/reg-event-db
 ::add-view
 (fn [db [_ {id paths/view-id-key :as view-desc}]]
   (assoc-in db
             (conj paths/all-views id)
             view-desc)))

(re-frame/reg-sub
 ::views
 (fn [db [_ sorted?]]
   (let [v (vals (get-in db paths/all-views {}))]
     (if sorted?
       (sort-by paths/view-order-key
                v)
       v))))

(re-frame/reg-sub
 ::current-view-content
 (fn [db _]
   (let [v-key (keyword (get-in db paths/current-view :settings))]
     (get-in db 
             (conj paths/all-views v-key paths/view-content-key)
             (fn []
               [:div.content "View with key doesn't exist" v-key])))))

(defn navigation []
  (let [views @(re-frame/subscribe [::views true])
        curr-view @(re-frame/subscribe [::current-view])]
    [mui/drawer {:variant "permanent"
                 :anchor "left"
                 :PaperProps {:style {:overflow :hidden}}}
     (apply conj
            [mui/mui-list {:style {:padding-top "64px"
                                   :width "52px"}}]
            (mapv (fn [{action-key paths/view-id-key
                        prim-text paths/view-label-key
                        icon paths/view-icon-key
                        display-fn paths/display-fn-key
                        content paths/view-content-key}]
                    [:div
                     [mui/divider]
                     [mui/listitem {:button true
                                    :key action-key
                                    :style {:min-width "0px"}
                                    :on-click #(cond
                                                 content (re-frame/dispatch [::switch-view action-key])
                                                 display-fn (display-fn))
                                    :selected (= curr-view action-key)}
                      [mui/tooltip {:title prim-text
                                    :placement :right}
                       [mui/listitem-icon {:style {:min-width "0px"}}
                        icon]]]])
                  views))
     [:div {:style {:bottom 10
                    :position :absolute
                    :width "100%"}}
      [:center
       [mui/typography {:style {:font-size 11
                                :color "rgb(51,153,153)"}}
        config/full-version]]]]))

(defn app-bar []
  (let [{:keys [status]} nil] ;API Connection status
    [mui/appbar {:position "fixed"
                 :style {:backgroundColor "rgb(0,0,0)"
                         :z-index 1201}}
     [mui/toolbar {:disableGutters true}
      [mui/svg-icon {:style {:padding-top "1px"
                             :padding-bottom "1px"
                             :margin-left "10px"
                             :padding-left "8px"
                             :padding-right "5px"
                             :display :inline
                             :background-color "white"
                             :border-radius "20px"
                             :cursor :pointer
                             :color (if status
                                      "rgb(0, 209, 102)"
                                      "rgb(240, 22, 62)")}
                     :titleAccess (if status
                                    "API connection okay"
                                    "No connection to api")
                     ;On-click open Website
                     :on-click #(let [url "http://google.com"]
                                  (when (and (not (clj-str/blank? url))
                                             (clj-str/includes? url "http"))
                                    (js/console.log "open url" url)
                                    (open-link url)))
                     :font-size :default}
       [mui/icon "Public"]]
      [mui/typography {:variant "h5"
                       :style {:color "white"
                               :padding-left "10px"}}
       config/app-name]
      [mui/card {:style {:position "absolute"
                         :right 10
                         :width "120px"
                         :height "50px"
                         :backgroundColor "rgba(255, 255, 255, 0.0)"}}
       [mui/card-media {:image "img/logo.png"
                        :style {:height "46px"
                                :width "100px"
                                :margin-left "10px"
                                :margin-top "2px"}}]]]]))

(defonce theme (mui/createMuiTheme
                (clj->js {:typography
                          {:useNextVariants true}})))

(defn starting-view []
  [:div
   [:div {:id "{{name}}_bg"}]
   [:div {:id "{{name}}_bg2"}]
   [:div {:id "{{name}}_left_accent"}]
   [mui/paper {:style {:position :absolute
                       :bottom 200
                       :width "100%"}
               :align "center"}
    (if @(re-frame/subscribe [:{{name}}.update-ui/update-checking-running?])
      [upd-ui/update-view]
      [components/loading "{{name}} is starting..."])]])

(defn {{name}} []
  [:div.main
   [components/status]
   [shortcut-view/dialog]
   (if @(re-frame/subscribe [::start-ui?])
     (let [view-component @(re-frame/subscribe [::current-view-content])]
       (js/console.log view-component)
       [mui/mui-theme-provider {:theme theme}
        [:div  {:style {:padding-top "64px"
                        :padding-left "40px"}}
         [app-bar]
         [navigation]
         [:div {:id "{{name}}_bg"}]
         [:div {:id "{{name}}_bg2"}]
         [:div {:id "{{name}}_left_accent"}]
         (when view-component
           [view-component])]])
     [starting-view])])
