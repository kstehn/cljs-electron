(ns {{name}}.views.welcome
  (:require [{{name}}.paths.views :as views-paths]
            [{{name}}.material-ui :as mui]
            [re-frame.core :as re-frame]))

(defn content []
  [:div.content
   [mui/typography; {:class "settings-header"}
    "Welcome"]])

(def view-desc {views-paths/view-id-key :welcome
                views-paths/view-label-key "Welcome"
                views-paths/view-icon-key [mui/icon "Eco" {:font-size :small
                                                           :color :disabled}]
                views-paths/view-content-key content
                views-paths/view-order-key 1})

(re-frame/dispatch [:{{name}}.views.main-view/add-view view-desc])