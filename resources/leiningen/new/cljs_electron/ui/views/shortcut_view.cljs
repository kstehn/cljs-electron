(ns {{name}}.views.shortcut-view
  (:require [re-frame.core :as re-frame]
            [{{name}}.material-ui :as mui]
            [{{name}}.utils.shortcuts :as shortcuts]
            [{{name}}.paths.shortcuts :as shortcuts-paths]
            [{{name}}.paths.views :as views-paths]
            [clojure.string :as clj-str]))

(re-frame/reg-sub
 ::show-dialog?
 (fn [db]
   (get-in db shortcuts-paths/show false)))

(re-frame/reg-event-db
 ::show-dialog
 (fn [db [_ flag]]
   (if flag
     (do
       (shortcuts/stop)
       (assoc-in db shortcuts-paths/show true))
     (do
       (shortcuts/resume)
       (dissoc db shortcuts-paths/root)))))

(re-frame/reg-event-db
 ::reset-to-default
 (fn [db]
   (let [default-bindings (shortcuts/get-default-bindings)
         actual-bindings (shortcuts/extract-bindings (shortcuts/get-shortcuts))
         only-diff (reduce (fn [res [k bind]]
                             (if (= bind (get actual-bindings k))
                               res
                               (assoc res k bind)))
                           {}
                           default-bindings)]
     (if (empty? only-diff)
       db
       (assoc-in db shortcuts-paths/edit-root only-diff)))))

(re-frame/reg-event-fx
 ::change-keybinding
 (fn [{db :db} [_ shortcut-id new-bind]]
   (let [edit-bindings (get-in db shortcuts-paths/edit-root {})
         is-same-binding? (shortcuts/is-same-binding? shortcut-id new-bind)
         already-exists? (or
                          (some (fn [[_ b]]
                                  (= new-bind b))
                                edit-bindings)
                          (shortcuts/binding-exist? new-bind (conj (vec (keys edit-bindings))
                                                                   shortcut-id)))]
     (cond
       already-exists?
       {:dispatch [:{{name}}.components.status/show-status-message (str "Combination " new-bind " already exist") :warn]}
       is-same-binding?
       {:db (update-in db shortcuts-paths/edit-root dissoc shortcut-id)}
       :else
       {:db (assoc-in db (shortcuts-paths/edit shortcut-id) new-bind)}))))

(re-frame/reg-sub
 ::changed-binding
 (fn [db [_ shortcut-id]]
   (if shortcut-id
     (get-in db (shortcuts-paths/edit shortcut-id))
     (get-in db shortcuts-paths/edit-root))))

(re-frame/reg-event-db
 ::save-bindings
 (fn [db]
   (let [bindings (get-in db shortcuts-paths/edit-root)]
     (when bindings
       (shortcuts/save-bindings bindings))
     db)))

(defn combination-view [shortcut-id {:keys [bind]}]
  (let [new-bind @(re-frame/subscribe [::changed-binding shortcut-id])]
    [mui/chip {:size :small
               :color (if new-bind
                        "primary"
                        "default")
               :onClick #(do
                           (re-frame/dispatch [:{{name}}.components.status/show-status-message "Recording.." :info])
                           (shortcuts/record-shortcut (fn [new-bind]
                                                        (re-frame/dispatch [::change-keybinding shortcut-id new-bind]))))
               :style {:margin-right "10px"
                       :opacity "0.7"}
                     ;:width "100px"}
               :label (-> (or new-bind bind "")
                          (clj-str/replace #"\+" " + "))}]))

(defn shortcut-view [[shortcut-id  {:keys [label] :as desc}]]
  [:tr
   [:td
    [combination-view shortcut-id desc]]
   [:td label]])

(defn dialog []
  (let [show? @(re-frame/subscribe [::show-dialog?])
        changed-bindings @(re-frame/subscribe [::changed-binding])]
    [mui/dialog {:open show?
                 :onClose #(re-frame/dispatch [::show-dialog false])}
     [mui/dialog-title {:id "shortcuts-dialog"}
      "Shortcuts"]
     [mui/dialog-content
      [mui/dialog-content-text (fn []
                                 {:variant "subtitle2"})
       "Here you can find all available shortcuts. To change: click on combination and record a new key-combination."]
      [:table
       (reduce (fn [par shortcut-desc]
                 (conj par [shortcut-view shortcut-desc]))

               [:tbody]
               (sort-by (fn [[_ d]]
                          (get d :order))
                        (shortcuts/get-shortcuts)))]]
     [mui/dialog-actions
      [mui/button {:variant :outlined
                   :style {:font-size "10px"
                           :position :absolute
                           :left 10}
                   :on-click #(re-frame/dispatch [::reset-to-default])}
       "Reset to default"]
      [mui/button {:color "secondary"
                   :on-click #(re-frame/dispatch [::show-dialog false])}
       "Cancel"]

      [mui/button {:color "primary"
                   :disabled (or (nil? changed-bindings)
                                 (empty? changed-bindings))
                   :on-click (fn []
                               (re-frame/dispatch [::save-bindings false])
                               (re-frame/dispatch [::show-dialog false]))}
       [mui/icon "Save"]
       "Save"]]]))

(def view-desc {views-paths/view-id-key :shortcuts
                views-paths/view-label-key "Shortcuts"
                views-paths/display-fn-key #(re-frame/dispatch [::show-dialog true])
                views-paths/view-icon-key [mui/icon "Keyboard" {:font-size :small
                                                                :color :disabled}]
                views-paths/view-order-key 2})

(re-frame/dispatch [:{{name}}.views.main-view/add-view view-desc])