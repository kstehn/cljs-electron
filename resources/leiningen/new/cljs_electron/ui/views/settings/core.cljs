(ns {{name}}.views.settings.core
  (:require [re-frame.core :as re-frame]
            [{{name}}.data :as data]
            [{{name}}.paths.views :as views-paths]
            [{{name}}.settings :as settings]
            [{{name}}.material-ui :as mui]
            [clojure.string :as cstr]
            [{{name}}.views.settings.general :as general-settings]
            [{{name}}.views.settings.util :as sutil]))

(re-frame/reg-event-fx
 ::save-settings-node
 (fn [{db :db} [_ settings-node kv-map]]
   (settings/update-setting settings-node kv-map)
   {:db (assoc-in db sutil/settings-save-path (merge
                                               (get-in db sutil/settings-save-path {})
                                               kv-map))
    :dispatch-n [[:{{name}}.components.status/show-status-message "Settings saved" :success]]}))

(re-frame/reg-sub
 ::get-settings-for-key
 (fn [db [_ settings-key]]
   (sutil/settings-for-key db settings-key)))

(defn save-settings [db settings-key value]
  (assoc-in db
            (conj sutil/settings-save-path settings-key)
            value))

(re-frame/reg-event-db
 ::settings
 (fn [db [_ settings-key value]]
   (save-settings db settings-key value)))

(def default-settings
  {:url ""
   :autoupdatecheck true})

(re-frame/reg-sub
 ::settings
 (fn [db _]
   (merge default-settings
          (get-in db sutil/settings-save-path
                  {:url ""}))))

(defn save-setting
  ([settings-node settings-key]
   (save-setting settings-node settings-key nil))
  ([settings-node settings-key d-value]
   (let [setting-val (get settings-node settings-key d-value)]
     (when (not (nil? setting-val))
       (re-frame/dispatch [::settings settings-key setting-val])))))

(defn settings-loaded [settings]
  (let [{:keys [general keybindings]} settings]
    (when keybindings
      (re-frame/dispatch [:{{name}}.utils.shortcuts/restore-keybindings keybindings]))
    (save-setting general :autoupdatecheck true)
    (when (:start-view general)
      (re-frame/dispatch [::settings :start-view (keyword (:start-view general))])
      (re-frame/dispatch [:{{name}}.views.main-view/switch-view (:start-view general)]))))

(defn content []
  [:div.content
   [general-settings/content]
   ;Add more settings if needed
   ])

(def view-desc {views-paths/view-id-key :settings
                views-paths/view-label-key "Settings"
                views-paths/view-icon-key [mui/icon "Settings" {:font-size :small
                                                                :color :disabled}]
                views-paths/view-content-key content
                views-paths/view-order-key 5000})

(re-frame/dispatch [:{{name}}.views.main-view/add-view view-desc])