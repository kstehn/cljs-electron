(ns {{name}}.views.settings.util
  (:require [re-frame.core :as re-frame]))

(def settings-save-path [:settings])

(defn settings-key-p [settings-key]
  (conj settings-save-path settings-key))

(defn settings-for-key [db settings-key]
  (get-in db (conj settings-save-path settings-key)))

(defn update-settings-kv
  ([k v]
   (update-settings-kv nil k v))
  ([save-event k v]
   (update-settings-kv save-event k nil v))
  ([save-event k old-default v]
   (when-not (= v old-default)
     (re-frame/dispatch [:{{name}}.views.settings.core/settings
                         k v])
     (when save-event
       (re-frame/dispatch save-event)))))

(defn update-keepvalues-settings [save-event old-options valid-options dissoc-val k v]
  (update-settings-kv
   save-event
   k
   (if dissoc-val
     (filterv (fn [{:keys [label]}]
                (not= label dissoc-val))
              old-options)
     (conj old-options {:label (aget v "target" "value")
                        :key (:key (first (filterv #(= (aget v "target" "value")
                                                       (:label %))
                                                   valid-options)))}))))

(defn update-settings-switch [save-event k v]
  (update-settings-kv save-event
                      k
                      (aget v "target" "checked")))

(defn update-settings-value
  ([k v]
   (update-settings-value nil k v))
  ([save-event k v]
   (update-settings-value save-event k "" v))
  ([save-event k old-default v]
   (let [val (aget v "target" "value")]
     (when (and (not= "" val)
                (not= old-default val))
       (update-settings-kv save-event
                           k
                           val)))))

(defn add-when-not-nil [kv-m]
  (into {} (filter (fn [[_ v]]
                     (not (nil? v)))
                   kv-m)))

(re-frame/reg-event-fx
 ::save-settings
 (fn [{db :db} [_ settings-node setting-keys save-events]] ;save-events [[<save-event> <setting-key>]]
   (let [all-settings (get-in db settings-save-path)
         node-settings (into {} (mapcat (fn [skey]
                                          {skey (get all-settings skey)})
                                        setting-keys))
         save-events (if save-events
                       (mapv (fn [[ev dkey]]
                               [ev (get all-settings dkey)])
                             save-events)
                       [])]
     {:dispatch-n (conj save-events
                        [:{{name}}.views.settings.core/save-settings-node
                         settings-node
                         (add-when-not-nil node-settings)])})))
