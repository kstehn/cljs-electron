(ns {{name}}.views.settings.general
  (:require [re-frame.core :as re-frame]
            [reagent.core :as reagent]
            [{{name}}.material-ui :as mui]
            [{{name}}.util :refer [send-ipc]]
            [{{name}}.views.settings.util :as sutil]
            [{{name}}.paths.views :as view-paths]))

(def settings-node :general)
(def autoupdate-key :autoupdatecheck)
(def startview-key :start-view)
(def setting-keys [autoupdate-key
                   startview-key])
(def save-event [::sutil/save-settings settings-node setting-keys])

(defn check-update-button []
  (let [java-available?  @(re-frame/subscribe [:{{name}}.utils.system-conditions/java-available?])
        is-mac? @(re-frame/subscribe [:{{name}}.utils.system-conditions/is-mac?])]

    [:div
     [mui/button {:variant "outlined"
                  :style {:margin-bottom "5px"}
                  :size "small"
                  :disabled (not (and java-available? (not is-mac?)))
                  :on-click #(re-frame/dispatch [:{{name}}.update-ui/run-check-dialog
                                                 (fn [] (send-ipc "check-update"))])}
      [mui/icon "GetApp"]
      "Check for update"]
     (when (and (not java-available?)
                (not is-mac?))
       [:div {:style {:font-size "12px"
                      :color "red"}}
        "You need java to use the update-function."])
     (when is-mac?
       [:div {:style {:font-size "12px"
                      :color "red"}}
        "No update-function for macOS. Need to manual install new version."])]))

(defn autoupdatecheck-switch []
  (let [autoupdatecheck @(re-frame/subscribe [:{{name}}.views.settings.core/get-settings-for-key autoupdate-key])]
    [mui/form-group
     [mui/form-control-label
      {:style {:margin-top "5px"}
       :control (reagent/as-element
                 [mui/switch {:checked autoupdatecheck
                              :size :small
                              :disabled (not @(re-frame/subscribe [:{{name}}.utils.system-conditions/updatefeature-available?]))
                              :onChange (partial sutil/update-settings-switch
                                                 save-event
                                                 autoupdate-key)}])

       :label (reagent/as-element
               [mui/typography {:style {:font-size "12px"}}
                "On startup check for Updates?"])}]]))

(defn content []
  (let [possible-views (filterv view-paths/view-content-key
                                @(re-frame/subscribe [:{{name}}.views.main-view/views true]))
        start-view @(re-frame/subscribe [:{{name}}.views.settings.core/get-settings-for-key startview-key])]
    [:div
     [mui/expansion-panel {:expanded true}
      [mui/expansion-panel-summary {:style {:cursor :default
                                            :min-height "40px"
                                            :height "40px"}}
       [mui/typography; {:class "settings-header"}
        "General"]]
      [mui/expansion-panel-details
       [:div {:style {:width "100%"}}
        [:div {:style {:display :flex
                       :margin-bottom "20px"}}
         [check-update-button]
         [:div {:style {:display :flex
                        :flexDirection :column
                        :width "40px"}}]
         [autoupdatecheck-switch]]
        [mui/select-component
         {:id "start-view-choose"
          :name "Start view"
          :value (if start-view
                   start-view
                   "")
          :on-change-func (fn [val]
                            (sutil/update-settings-value
                             save-event
                             startview-key
                             start-view
                             val))
          :options (mui/vec->options possible-views 
                                     view-paths/view-label-key 
                                     view-paths/view-id-key)
          :helper-text "View that should be active on start."}]]]]]))
