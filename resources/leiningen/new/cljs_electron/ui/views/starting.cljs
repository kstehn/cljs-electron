(ns {{name}}.views.starting
 (:require [re-frame.core :as re-frame]
           [{{name}}.util :refer [on-ipc get-url send-ipc]]
           [{{name}}.views.settings.core :as ui-settings]
           [{{name}}.worker :as worker]))

(enable-console-print!)

;Add more if other processes needs to be done beforehand
(def start-processes [:java-checked :settings-loaded]) 

(re-frame/reg-event-fx
 ::system-conditions
 (fn [_ [_ res]]
  (worker/message! "load-settings")
  {:dispatch-n [[:{{name}}.utils.system-conditions/system-conditions (js->clj res :keywordize-keys true)]
                [::finished-start-process :java-checked]]}))

(re-frame/reg-event-fx
 ::finished-start-process
 (fn [{db :db} [_ process]]
  (let [processes (get-in db [::start-processes] [])
        new-processes (conj processes process)]
      (if (or (get-in db [:start-ui] false)
              (= (set new-processes)
                 (set start-processes)))
        {:db (dissoc db ::start-processes)
         :dispatch [:{{name}}.views.main-view/start-ui]}
        {:db (assoc-in db [::start-processes] new-processes)}))))

(defn settings-loaded [event settings]
 (let [settings (js->clj settings :keywordize-keys true)]
   (ui-settings/settings-loaded settings)
   (re-frame/dispatch [::finished-start-process :settings-loaded])
   ;Über Subscribe leider zu langsam, sodass hier nur nil rauskommen würde. TODO Evtl. auf Settings-util umbiegen, um den Pfad hier nicht hart drinne zu haben
   (when (and
          @(re-frame/subscribe [:{{name}}.utils.system-conditions/updatefeature-available?])
          (get-in settings [:general :autoupdatecheck] true))
     (re-frame/dispatch [:{{name}}.update-ui/run-check (fn [] (send-ipc "check-update"))]))))
