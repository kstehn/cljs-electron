(ns {{name}}.utils.queue)

(def queue :queue-events)

(defn push [db id event]
  (update-in db [queue id] conj event))

(defn pop [db id]
  (update-in db [queue id] rest))

(defn dispatch-event [db id]
  (first (get-in db [queue id])))

(defn more-in-queue? [db id]
  (> (count (get-in db [queue id]))
     0))

(defn clean-queue [db id]
  (update db queue dissoc id))