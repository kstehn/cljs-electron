(ns {{name}}.worker-core
  (:require [re-frame.core :as re-frame]
            [{{name}}.util :refer [on-ipc]]
            [{{name}}.worker-settings :as settings]
            [{{name}}.example :as example]))

(enable-console-print!)

(re-frame/reg-event-db
 ::initialize-db
 (fn  [_ _]
   {:queue-events {}}))

(on-ipc "update-settings" settings/update-settings)
(on-ipc "load-settings" settings/load-settings)
(on-ipc "example" example/long-running-example)

(defn -main []
  (re-frame/dispatch-sync [::initialize-db])
  (re-frame/clear-subscription-cache!))

(set! *main-cli-fn* -main)
