(ns {{name}}.example
  (:require [re-frame.core :as re-frame]
            [ajax.core :as ajax]
            [day8.re-frame.http-fx]
            [{{name}}.utils.queue :as queue]
            [{{name}}.util :refer [web-contents calc-offsets get-path send-all]]
            [{{name}}.data :as data-util]))

(def queue-id ::example) ; Use queue when you need to dispatch multiple events one after the other

(re-frame/reg-event-fx
 ::done-running
 (fn [{db :db} [_]]
   (send-all "example-done" (clj->js {:result (str (random-uuid))}))
   {:db db}; Maybe do some clean up afterwards
   ))

(re-frame/reg-event-fx
 ::check-and-clean-queue
 (fn [{db :db} [_ next-event]]
   (let [req-id (str (random-uuid))]
     {:db (-> db
              (queue/clean-queue queue-id)
              (assoc-in [:example :reg-id] req-id))
      :dispatch (conj next-event req-id)})))

(re-frame/reg-event-db
 ::save-web-contents-caller
 (fn [db [_ caller]]
   (assoc db ::example-caller caller)))

(re-frame/reg-event-fx 
 ::some-long-running-task
 (fn [_ [_ params]]
   (js/console.log "params" params)
   {:dispatch [::done-running]}))

(defn long-running-example
  "This shows how to handle long running tasks
   like multiple HTTP calls"
  [event id params-map]
  (let [sender (aget event "sender")
        conv-params-map (js->clj params-map :keywordize-keys true)]
    (re-frame/dispatch [::check-and-clean-queue
                        [::some-long-running-task conv-params-map]])
    (re-frame/dispatch [::save-web-contents-caller sender])))