# {{name}}



## Features
* TODO

## Wishlist
* TODO

## Requirements

* leiningen 2.6.x +
* node v0.12.x +

Run the first time
```
$ npm install
```

## development mode

development mode use figwheel. 
```
$ lein start
```
Open other terminal window.

For Windows:
```
$ lein electron-win
```

For Linux:
```
$ lein electron-ux
```

Debuging with devtron execute in console (in RedLorry)
```
require('devtron').install()
```
You should now see a Devtron tab added to the DevTools

## Package App (Does not work right now!)

### (If not already installed Electron-packager.)

```
$ npm install -g electron-packager
```

### run command

#### for windows 64bit app

```
$ lein uberapp-win64
```