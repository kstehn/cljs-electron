(defproject {{name}} "0.0.0"
  :description "Beatlist in clojurescript"
  :url ""
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}

  :min-lein-version "2.7.1"

  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/clojurescript "1.10.753" :exclusions [org.apache.ant/ant]]
                 [org.clojure/core.async "0.5.527"]
                 [re-frame "1.0.0"]
                 [cljsjs/nodejs-externs "1.0.4-1"]
                 [cljs-http "0.1.46"]
                 [day8.re-frame/http-fx "0.1.6"]
                 [re-frame-utils "0.1.0"]
                 [markdown-to-hiccup "0.6.2"]
                 [cljs-ajax "0.8.0"]
                 [cljsjs/material-ui "4.10.2-0"]
                 [cljsjs/material-ui-icons "4.4.1-0"]
                 [cljsjs/mousetrap-plugins "1.6.3-0"]
                 [com.taoensso/timbre "4.10.0"]
                 [version-clj "0.1.2"]]

  :managed-dependencies [[cljsjs/react "16.13.1-0"]
                         [cljsjs/react-dom "16.13.1-0"]
                         [cljsjs/react-dom-server "16.13.1-0"]
                         [cljsjs/create-react-class "15.6.3-1"]
                         [reagent "0.7.0"]] ;Reagent 0.8.0+ is breaking the application, invalid react hook call...

  :plugins [[lein-cljsbuild "1.1.7" :exclusions [[org.clojure/clojure]]]
            [lein-shell "0.5.0"]
            [lein-externs "0.1.6"]
            [lein-figwheel "0.5.19" :exclusions [org.clojure/core.cache]]
            [lein-exec "0.3.7"]]

  :source-paths ["src"]

  :cljsbuild {:builds
              [{:id           "main-dev"
                :source-paths ["src/main" "src/shared"]
                :figwheel     {:on-jsload "{{name}}.main-core/reload"}
                :compiler     {:main           {{name}}.main-core
                               :output-to      "resources/main.js"
                               :output-dir     "resources/public/js/main-dev"
                               :optimizations  :none
                               :install-deps   true
                               :target         :nodejs
                               :parallel-build true
                               :source-map     true
                               :npm-deps       {:electron "9.2.0"
                                                :ws       "7.2.0"}}}

               {:id           "main"
                :source-paths ["src/main" "src/shared"]
                :compiler     {:main           {{name}}.main-core
                               :optimizations  :simple
                               :output-to      "resources/main.js"
                               :output-dir     "resources/app/public/js/main-out"
                               :install-deps   true
                               :target         :nodejs
                               :parallel-build true
                               :process-shim   true
                               :pretty-print   false
                               :npm-deps       {:electron "9.2.0"}}}

               {:id           "ui-dev"
                :source-paths ["src/ui" "src/shared"]
                :figwheel     true
                :compiler     {:main           {{name}}.ui-core
                               :output-to      "resources/public/js/ui.js"
                               :output-dir     "resources/public/js/ui-dev"
                               :closure-defines {"re_frame.trace.trace_enabled_QMARK_" true}
                               :preloads [;day8.re-frame-10x.preload
                                          devtools.preload]
                               :optimizations  :none
                               :install-deps   true
                               :hashbang       false
                               :target         :nodejs
                               :parallel-build true
                               :source-map     true
                               :npm-deps       {:react                 "16.13.1"
                                                :react-dom             "16.13.1"
                                                :create-react-class    "15.6.3"
                                                :electron              "9.2.0"
                                                :electron-default-menu "1.0.1"}}}

               {:id           "ui"
                :source-paths ["src/ui" "src/shared"]
                :compiler     {:main           {{name}}.ui-core
                               :optimizations  :simple
                               :output-to      "resources/app/public/js/ui.js"
                               :output-dir     "resources/app/public/js/ui-out"
                               :install-deps   true
                               :target         :nodejs
                               :hashbang       false
                               :parallel-build true
                               :process-shim   true
                               :pretty-print   false
                               :npm-deps       {:react                 "16.13.1"
                                                :react-dom             "16.13.1"
                                                :electron              "9.2.0"
                                                :electron-default-menu "1.0.1"
                                                :create-react-class    "15.6.3"}}}

               {:id           "worker-dev"
                :source-paths ["src/worker" "src/shared"]
                :figwheel     true
                :compiler     {:main           {{name}}.worker-core
                               :output-to      "resources/public/js/worker.js"
                               :output-dir     "resources/public/js/worker-dev"
                               :optimizations  :none
                               :preloads [;day8.re-frame-10x.preload
                                          devtools.preload]
                               :install-deps   true
                               :hashbang       false
                               :target         :nodejs
                               :parallel-build true
                               :npm-deps       {:electron "9.2.0"}}}


               {:id           "worker"
                :source-paths ["src/worker" "src/shared"]
                :compiler     {:main           {{name}}.worker-core
                               :optimizations  :simple
                               :output-to      "resources/app/public/js/worker.js"
                               :output-dir     "resources/app/public/js/worker-out"
                               :install-deps   true
                               :hashbang       false
                               :process-shim   true
                               :target         :nodejs
                               :parallel-build true
                               :npm-deps       {:electron "9.2.0"}}}]}

  :aliases {"electron-win" ["shell" "node_modules/.bin/electron.cmd" "./resources/main.js"]
            "electron-ux" ["shell" "node_modules/.bin/electron" "./resources/main.js"]
            "build"    ["do" "clean" ["cljsbuild" "once" "main" "worker" "ui"]]
            "start"    ["do" "clean" ["figwheel" "main-dev" "worker-dev" "ui-dev"]]
            "prepare-release" ["do"
                               ["shell" "mkdir" "prepared/"]
                               ["shell" "cp" "-R" "resources/public" "prepared/"]
                               ["shell" "mkdir" "prepared/public/js/"]
                               ["shell" "cp" "-R" "resources/app/public/js/worker.js" "prepared/public/js/"]
                               ["shell" "cp" "-R" "resources/app/public/js/ui.js" "prepared/public/js/"]
                               ["shell" "cp" "resources/main.js" "prepared/main.js"]
                               ["shell" "cp" "package.json" "prepared/package.json"]]
            "clean-up-release" ["do"
                                ["shell" "rm" "-rf" "prepared"]]}
  :figwheel {:css-dirs         ["resources/public/css"]
             :nrepl-port       7888
             :nrepl-middleware ["cider.nrepl/cider-middleware"
                                "cemerick.piggieback/wrap-cljs-repl"]}

  ;; Setting up nREPL for Figwheel and ClojureScript dev
  ;; Please see:
  ;; https://github.com/bhauman/lein-figwheel/wiki/Using-the-Figwheel-REPL-within-NRepl
  :profiles {:dev {:dependencies [[cider/piggieback "0.4.2"]
                                  [figwheel-sidecar "0.5.19"]
                              ;    [day8.re-frame/re-frame-10x "0.3.7"]
                                  [binaryage/devtools "0.9.11"]]
                   ;; need to add dev source path here to get user.clj loaded
                   :source-paths ["src/ui" "src/main" "src/worker" "dev"]
                   :plugins      [[cider/cider-nrepl "0.21.1"]]
                   :repl-options {:nrepl-middleware [cider.piggieback/wrap-cljs-repl]}

                   :clean-targets ^{:protect false} ["resources/main.js"
                                                     "resources/public/js"
                                                     :target-path]}})
