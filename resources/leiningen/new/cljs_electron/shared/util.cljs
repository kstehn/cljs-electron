(ns {{name}}.util
  "A namespace to hide interop details and electron use."
  (:require [electron :refer [BrowserWindow remote app ipcRenderer ipcMain shell]]
            [os :as os]
            [path]
            [child_process :as child-proc]
            [clojure.string :as string]))

(defonce app-title "{{name}}") ; to find the frame again

(defonce java-version (atom nil))
(defonce platform (atom nil))

(defn get-os []
  (when-not @platform
    (reset! platform (.platform os)))
  ;(println "Found OS" @platform))
  @platform)

(defn is-java-installed? []
  (when-not @java-version
    (let [spawn (.spawnSync child-proc
                            "java"
                            (clj->js ["-version"]))
          res-str (.toString (aget spawn "stderr"))] ;for some reason the result is in stderr
      (reset! java-version (second (string/split res-str  "\""))))
     ; (println "Found Java Version " @java-version))
    (and (not (nil? @java-version))
         (not= "" (string/replace @java-version " " "")))))

(defn dissoc-in
  "Dissociates an entry from a nested associative structure returning a new
  nested structure. keys is a sequence of keys. Any empty maps that result
  will not be present in the new structure."
  [m [k & ks :as keys]]
  (if ks
    (if-let [nextmap (get m k)]
      (let [newmap (dissoc-in nextmap ks)]
        (if (seq newmap)
          (assoc m k newmap)
          (dissoc m k)))
      m)
    (dissoc m k)))

(defn calc-offsets [limit total-count]
  (let [needed-steps (.ceil js/Math (/ (- total-count limit)
                                       limit))]
    (mapv #(* % limit)
          (range 1 (+ needed-steps 1)))))

(defn get-app []
  (or app (aget remote "app")))

(defn get-path
  [path-id filename]
  (-> (get-app)
      (.getPath path-id)
      (path/join filename)))

(defn- browser-window-ctor []
  (or BrowserWindow (aget remote "BrowserWindow")))

(defn browser-window
  [options]
  (let [ctor (browser-window-ctor)]
    (ctor. (clj->js options))))

(defn is-prod? []
  (let [env (aget js/process "env" "NODE_ENV")
        exec-path (aget js/process "execPath")]
    (if env
      (= env "production")
      (not (string/includes? exec-path "node_modules")))))

(defn static-file-path [filename]
  (let [dirname js/__dirname
        public? (string/includes? dirname "public")
        prefix  (if public? "/" "/public/")] ;ugly but works xD
    (if (is-prod?)
      (str dirname prefix filename)
      (->> (path/join dirname "../../../" filename)
           (path/resolve)))))

(defn get-url [filename]
  (str "file://" (static-file-path filename)))

(defn load-url
  [window filename]
  (let [file-url (get-url filename)]
    (.loadURL window file-url)))

(defn on
  [emitter event-name handler]
  (.on emitter event-name handler))

(defn on-app
  [event-name handler]
  (on (get-app) event-name handler))

(defn once
  [window event function]
  (.once window event function))

(defn show
  [window]
  (.show window))

(defn focus
  [window]
  (.focus window))

(defn on-ipc
  "register ipc listener for renderer processes"
  [event-name handler]
  (on ipcRenderer event-name handler))

(defn send-ipc
  ([event-name data]
   (.send ipcRenderer event-name data))
  ([event-name]
   (.send ipcRenderer event-name)))

(defn on-ipc-main
  "register ipc listener for main process - explicitly named as
  it is the exception here"
  [event-name handler]
  (on ipcMain event-name handler))

(defn quit []
  (.quit app))

(defn relaunch []
  (.relaunch app))

(defonce window-id (atom nil))

(defn current-window-id []
  (if @window-id
    @window-id
    (let [windows  (->> (browser-window-ctor)
                        .getAllWindows)
          app-window (first (filterv (fn [window]
                                       (= app-title (.getTitle window)))
                                     windows))]
      (reset! window-id (aget app-window "id")))))

(defn save-dialog [options]
  (.showSaveDialogSync (aget remote "dialog") (clj->js options)))

(defn open-dialog [options]
  (.showOpenDialogSync (aget remote "dialog") (clj->js options)))

(defn open-file [file]
  (.openItem shell file))

(defn open-link [link]
  (.openExternal shell link))

(defn web-contents
  "Get the webContents of a browser window identified by id"
  [id]
  (aget
   (.fromId (browser-window-ctor)
            id)
   "webContents"))

(defn send-all
  ([event]
   (let [windows  (->> (browser-window-ctor)
                       .getAllWindows)]
     (doseq [win windows]
       (. win (send event)))))
  ([event content]
   (let [windows  (->> (browser-window-ctor)
                       .getAllWindows)]
     (doseq [win windows]
       (. win (send event content))))))

(defn destroy-current []
  (when remote
    (let [curr-win (.getCurrentWindow remote)]
      (when (and curr-win (not (.isDestroyed curr-win)))
        (.destroy curr-win)))))

(defn close-current []
  (when remote
    (let [curr-win (.getCurrentWindow remote)]
      (when (and curr-win (not (.isDestroyed curr-win)))
        (.close curr-win)))))
