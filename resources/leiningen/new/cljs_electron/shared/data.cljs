(ns {{name}}.data
  (:require [fs :refer [writeFileSync readFileSync]]
            [{{name}}.util :refer [get-path get-app]]))

(defn db-path
  "In Dev and Win: %APPDATA%/Electron
   In Prod and Win: %APPDATE%/{{name}}"
  []
  (get-path "userData" "{{name}}.json"))

(defn json-parse
  [string]
  (.parse js/JSON string))

(defn json-stringify
  [data]
  (.stringify js/JSON data))

(defn read
  ([path]
   (try
     (-> path
         readFileSync
         json-parse
         (js->clj :keywordize-keys true))
     (catch js/Object e
       {})))
  ([]
   (read (db-path))))

(defn- write-file-sync
  [data path]
  (writeFileSync path data))

(defn write
  [data]
  (println (db-path))
  (-> (read)
      (merge data)
      clj->js
      json-stringify
      (write-file-sync (db-path))))

(defn write-path
  [data path]
  (-> data
      clj->js
      json-stringify
      (write-file-sync path)))