(defproject update-helper "0.1.0"
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/core.async "0.4.490"]
                 [me.raynes/fs "1.4.6"]
                 [seesaw "1.5.0"]
                 [clj-http "3.9.1"]]
  :main ^:skip-aot update-helper.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  :uberjar-name "update-helper.jar")
