(ns update-helper.downloader
  (:require [clj-http.client :as http]
            [clojure.java.io :refer [output-stream]])
  (:import (org.apache.commons.io.input CountingInputStream)))

(defn insert-at [v idx val]
  "Addes value into a vector at an specific index."
  (-> (subvec v 0 idx)
      (conj val)
      (into (subvec v idx))))

(defn insert-after [v needle val]
  "Finds an item into a vector and adds val just after it.
   If needle is not found, the input vector will be returned."
  (let [index (.indexOf v needle)]
    (if (neg? index)
      v
      (insert-at v (inc index) val))))

(defn wrap-downloaded-bytes-counter
  "Middleware that provides an CountingInputStream wrapping the stream output"
  [client]
  (fn [req]
    (let [resp (client req)
          counter (CountingInputStream. (:body resp))]
      (merge resp {:body                     counter
                   :downloaded-bytes-counter counter}))))

(defn download-update [update-url progress-update-fn]
  (http/with-middleware
    (conj http/default-middleware http/wrap-lower-case-headers)
    (let [request (http/get update-url {:as :stream :decompress-body false})
          length (Integer. (get-in request [:headers "content-length"]))
          ;total-in-mb (/ length 1048576)
          cur-length (atom 0)
          buffer-size 1024]
      (with-open [input (->> request
                             :body
                             (CountingInputStream.))
                  output (output-stream "update.zip")]
        (let [buffer (make-array Byte/TYPE buffer-size)]
          (loop []
            (let [size (.read input buffer)]
              (when (pos? size)
                (swap! cur-length + size)
                (.write output buffer 0 size)
                (progress-update-fn (double (/ (* 100 @cur-length)
                                               length)))
                (recur)))))))))