(ns update-helper.core
  (:require [me.raynes.fs :as fs]
            [clojure.core.async :as async]
            [clojure.string :as string]
            [update-helper.swing-ui :as ui]
            [update-helper.downloader :as downloader]
            [update-helper.unzipper :as unzipper])
  (:import java.lang.Runtime)
  (:gen-class))

(def file-blacklist #{"update-helper.jar" "update.zip" "update-extracted"})

(defn is-windows? []
  (string/includes? (System/getProperty "os.name")
                    "Windows"))

(defn clean-install-folder [install-folder]
  (let [install-folder (fs/file ".")
        paths (fs/iterate-dir install-folder)
        [_ root-dirs root-files] (first paths)]
    (println "Clean install folder" (into root-dirs root-files))
    (doseq [path (into root-dirs root-files)]
      (when (not (file-blacklist path))
        (fs/delete-dir path)))))

(defn copy-update-to-install [install-folder update-folder]
  (println "Copy update to install folder")
  (fs/copy-dir-into update-folder install-folder))

(defn clean-update-files [update-folder]
  (println "Remove update files")
  (fs/delete-dir update-folder)
  (fs/delete (fs/file "." "update.zip")))

(defn finalize-update [install-folder update-folder]
  (ui/update-unzip-progress 100)
  (ui/update-finalize-progress -1)
  (println "Start to install update")
  (clean-install-folder install-folder)
  (copy-update-to-install install-folder update-folder)
  (clean-update-files update-folder)
  (println "Update Complete")
  (ui/update-finalize-progress 100)
  (. (Runtime/getRuntime) exec (if (is-windows?)
                                   ".\\{{name}}.exe"
                                   ".\\{{name}}")))

(defn download-update [update-url]
  (downloader/download-update
   update-url
   ui/update-download-progress))

(defn unzip-update [target-folder]
  (unzipper/unzip-update
   (fs/file "." "update.zip")
   target-folder
   ui/update-unzip-progress))

(defn -main
  [& args]
  (let [install-folder (fs/file ".")
        update-folder (fs/file "." "update-extracted")]
    (ui/start-ui)
    (download-update (first args))
    (unzip-update update-folder)
    (finalize-update install-folder update-folder)
    (java.lang.System/exit 0)))
