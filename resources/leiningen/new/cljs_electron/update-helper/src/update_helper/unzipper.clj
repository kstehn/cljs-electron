(ns update-helper.unzipper
  (:require [clojure.java.io :as io])
  (:import [java.util.zip ZipFile ZipEntry]))

(defn unzip-update [source-zip target-dir progress-update-fn]
  (with-open [zip-file (ZipFile. source-zip)]
    (let [target-path (.getAbsolutePath target-dir)
          total-entries (.size zip-file)
          cur-num-entries (atom 0)]
      (doseq [cur-entry (enumeration-seq (.entries zip-file))]
        (let [in-stream (.getInputStream zip-file cur-entry)
              local-file (io/file target-path (.getName cur-entry))]
          (io/make-parents local-file)
          (if (.isDirectory cur-entry)
            (do 
              (.mkdir local-file)
              (swap! cur-num-entries inc)
              (progress-update-fn (double (/ (* 100 @cur-num-entries)
                                             total-entries))))
            (with-open [out-stream (io/output-stream local-file)]
              (swap! cur-num-entries inc)
              (progress-update-fn (double (/ (* 100 @cur-num-entries)
                                             total-entries)))
              (io/copy in-stream out-stream))))))))