(ns {{name}}.auto-updater.util
  (:require [clojure.string :as string]
            [child_process :as child-proc]))

(def file-pattern (re-pattern #"\[(.*)\]"))

(defn file-str->file-tuple
  "Returns a tuple contaning the filename and the url."
  [file-str]
  (let [file-str (string/replace file-str "* " "")
        [matched-str file-name] (re-find file-pattern file-str)
        file-url (-> file-str
                     (string/replace matched-str "")
                     (string/replace "(" "")
                     (string/replace ")" ""))]
    [file-name file-url]))

(defn find-files
  "Returns all files found in the release description.
   {<filename> <markdownString>}"
  [files-str]
  (if files-str
    (let [files (filter #(not (empty? %))
                        (string/split files-str #"\n\n"))]
      (into {}
            (mapv file-str->file-tuple
                  files)))
    {}))

(defn split-to-notes-files
  "Returns the Release description in the form
   {:notes <str>
    :files {<filename> <markdownString>}}"
  [release-desc]
  (if release-desc
    (let [[release-notes files-str] (string/split release-desc #"\n\n---\n\n")
          files (find-files files-str)]
      {:notes release-notes
       :files files})
    {:notes ""
     :files {}}))

(defn install-update [download-url]
  (let [command (str "java -jar update-helper.jar " download-url)]
    (.unref (.spawn child-proc
                    command
                    (clj->js {:maxBuffer (* 1000
                                            1024)
                              :shell true
                              :detached true
                              :stdio "ignore"
                              :windowsHide true})))))
