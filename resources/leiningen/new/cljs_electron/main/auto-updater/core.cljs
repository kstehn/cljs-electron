(ns {{name}}.auto-updater.core
  (:require [version-clj.core :as version-check]
            [clojure.string :as string]
            [{{name}}.util :as util]
            [{{name}}.auto-updater.util :as updater-util]
            [{{name}}.auto-updater.api :as gitlab-api]
            [electron :refer [app]]
            [path]
            [fs]))

(defonce version (atom nil))
(defonce prod? (when app 
                 (aget app "isPackaged")))

(defn set-version [nversion]
  (reset! version nversion))

(defn release-version [tag]
  (if (string/starts-with? tag "alpha-")
    (let [[major minor alpha] (-> tag
                                  (string/replace "alpha-" "")
                                  (string/split #"\."))]
      (str major "." minor "-alpha" alpha))
    tag))

(defn fetch-release-callback [[{tag :tag_name}] failed?]
  (println "release-callback")
  (if failed?
    (util/send-all "currversion-is-newest")
    (let [release-version (release-version tag)
          newer-version? (= 1
                            (version-check/version-compare release-version @version))
          release-notess (-> @gitlab-api/current-release
                             (get :description)
                             updater-util/split-to-notes-files
                             :notes)]
      (if (and prod?
               newer-version?)
        (do (println "Update application")
            (util/send-all "update-available"
                           (clj->js {:version release-version
                                     :release-notes release-notess})))
        (util/send-all "currversion-is-newest")))))

(defn check-new-version []
  (gitlab-api/fetch-release fetch-release-callback))

(defn check-new-update-helper []
  (let [new-updater-jar (-> "new-update-helper.jar"
                            (path/join)
                            (path/resolve))
        target-updater-jar (-> "update-helper.jar"
                            (path/join)
                            (path/resolve))]
    (when (and prod?
               (.existsSync fs new-updater-jar))
      (.renameSync fs new-updater-jar target-updater-jar))))

(defn install-update []
  (let [update-url (gitlab-api/update-url)]
   (gitlab-api/is-host-reachable
    (fn [reachable?] 
      (if (and update-url reachable?)
        (do (updater-util/install-update update-url)
            (util/quit))
        (util/send-all "update-install-error"))))))
