(ns {{name}}.auto-updater.config
  (:require [{{name}}.util :as util]
            [{{name}}.data :as data-util]))

(defonce update-config-file  (-> "update-config.json"
                                 util/static-file-path
                                 data-util/read))

(def is-valid? (not-empty update-config-file))

(defn attribute [key]
  (when is-valid?
    (get update-config-file key)))

(def host (attribute :gitlab-host))
(def repository (attribute :repository))
(def project-id (attribute :project-id))

(def api-url (str host "api/v4/projects/" project-id))
(def is-windows? (= "win32"
                    (util/get-os)))
