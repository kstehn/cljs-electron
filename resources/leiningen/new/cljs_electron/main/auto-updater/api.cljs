(ns {{name}}.auto-updater.api
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require [cljs-http.client :as cljs-http]
            [cljs.core.async :refer [<! chan]]
            [{{name}}.data :as data-util]
            [ajax.core]
            [{{name}}.auto-updater.util :as updater-util]
            [{{name}}.auto-updater.config :as config]))

(defonce current-release (atom nil)) ;Will cache the latest release response from the gitlab server
(defonce latest-info (atom nil))

(defn download-latest-info []
  (go
    (try
      (let [latest-url (as-> current-release $
                         (deref $)
                         (get $ :description)
                         (updater-util/split-to-notes-files $)
                         (:files $)
                         (get $ "latest.json")
                         (str config/host config/repository $))
            resp  (-> latest-url
                      cljs-http/get
                      <!
                      :body
                      data-util/json-parse
                      (js->clj :keywordize-keys true))]
        (reset! latest-info resp))
      (catch :default e))))

(defn is-host-reachable [callback-fn]
  (go
    (try
      (let [resp  (<! (cljs-http/get (str config/api-url)))]
        (callback-fn (:success resp)))
      (catch :default e
       (callback-fn false)))))

(defn fetch-release
  "Fetch the latest release and saves it in the current-release atom."
  [callback]
  (go
   (try 
     (let [resp (:body (<! (cljs-http/get (str config/api-url "/releases/"))))]
       (reset! current-release (-> resp
                                   first
                                   (select-keys [:description :tag_name])))
       (download-latest-info)
       (when callback
         (callback resp false)))
    (catch :default e
      (callback nil true)))))

(defn update-url []
  (if config/is-windows?
    (get @latest-info :windows)
    (get @latest-info :linux)))