(ns {{name}}.main-core
  (:require [cljs.nodejs :as nodejs]
            [electron :refer [app Menu shell]]
            [electron-default-menu]
            [{{name}}.util :as u]
            [{{name}}.auto-updater.core :as auto-updater]))

(nodejs/enable-util-print!)

(defonce *main-window (atom nil))
(defonce *loading-window (atom nil))

(defn no-menu []
  (.setApplicationMenu app nil))

(defn default-menu []
  (let [menu (electron-default-menu app shell)]
    (->> menu
         (.buildFromTemplate Menu)
         (.setApplicationMenu Menu))))

(defn create-window []
  (let [window-options {:webPreferences {:nodeIntegration true
                                         :enableRemoteModule true
                                         :worldSafeExecuteJavaScript true}
                        :minWidth     850
                        :minHeight    750
                        :height 830
                        :autoHideMenuBar true
                      ;  :titleBarStyle "customButtonsOnHover"
                        ;:transparent true
                      ;  :frame false 
                        :resizable true
                        :show false
                        :title u/app-title}
        main-window (u/browser-window window-options)]
    (u/load-url main-window "index.html")
    (reset! *main-window  main-window)
    (u/once @*main-window "ready-to-show" (fn []
                                            (u/show @*main-window)
                                            (u/focus @*main-window)
                                            (when  @*loading-window
                                              (.close @*loading-window))))
    (u/on @*main-window "closed" #(reset! *main-window nil))
    ;(default-menu)
    #_(if (u/is-prod?)
        (no-menu)
        (default-menu))
    (reset! *loading-window (u/browser-window  {:webPreferences {:nodeIntegration true}
                                                :width     300
                                                :height    300
                                                :frame false
                                                :show false
                                                :toolbar false
                                                :transparent true
                                                :parent @*main-window}))
    (u/load-url @*loading-window "loading.html")
    (u/on @*loading-window "closed" #(reset! *loading-window nil))
    (u/once @*loading-window "ready-to-show" (fn []
                                               (u/show @*loading-window)))))

(defn quit-app []
  (when-not (= "darwin" (u/get-os))
    (u/quit)))

(defn activate []
  (when (nil? @*main-window)
    (create-window)))

(defn reload []
  (when @*main-window
    (.close @*main-window)
    (reset! *main-window nil)
    (when @*loading-window
      (.close @*loading-window)
      (reset! *loading-window nil))
    (u/relaunch)
    (u/quit)))

(defn -main []
  (u/on-ipc-main "check-update" (fn []
                                  (auto-updater/check-new-update-helper)
                                  (auto-updater/set-version (.getVersion app))
                                  ;(println "check-update")
                                  (auto-updater/check-new-version)))

  (u/on-ipc-main "install-update" (fn []
                                    (auto-updater/install-update)))
  (u/on-ipc-main "get-system-conditions" (fn []
                                           (. (aget @*main-window "webContents")
                                              (send
                                               "system-conditions"
                                               (clj->js {:java-available? (u/is-java-installed?)
                                                         :os (u/get-os)})))))

  (u/on-app "ready" create-window)
  (u/on-app "window-all-closed" quit-app)
  (u/on-app "activate" activate))

(set! *main-cli-fn* -main)
