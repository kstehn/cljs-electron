(defproject cljs-electron/lein-template "0.2.0"
  :description "Electron skeleton with re-frame and worker process"
  :url "https://gitlab.com/kstehn/cljs-electron"
  :license {:name "The MIT License (MIT)"
            :url "https://mit-license.org"}
  :eval-in-leiningen true
  :dependencies [[clj-jgit "1.0.0-beta3"]]
  :deploy-repositories [["releases" :clojars]
                        ["snapshots" :clojars]])
